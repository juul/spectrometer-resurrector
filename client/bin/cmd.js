#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
const { SerialPort } = require('serialport');
const { ReadlineParser } = require('@serialport/parser-readline')
var minimist = require('minimist');
var xtend = require('xtend');

var csvHeader = "Wavelength,Sample,Reference"
var curWavelength = undefined;

var argv = minimist(process.argv.slice(2), {
  alias: {
    D: 'debug',
    d: 'device',
    p: 'plot',
    o: 'output',
    s: 'settings',
    h: 'help'
  },
  boolean: [
    'debug',
    'help',
    'plot'
  ],
  default: {
    settings: '../settings.js',
    debug: false,
    device: '/dev/ttyUSB0'
  }
});

function usage(p) {
  if(!p) p = console.error;
  p("Usage:", path.basename(__filename), "<command> [arg1] [arg2] [...]");
  p('');
  p('  Commands:');
  p('');
  p("    goto <wavelength>");
  p("      Move mirror to measure at <wavelength> nm");
  p('');
  p("    gotop <wavelength>");
  p("      Precise version of `goto` which always re-homes and always");
  p("      approaches the location from the same side in an attempt");
  p("      to minimize the effects of backlash");
  p('');
  p("    read");
  p("      Read the current light output.");
  p("      Outputs from both the reference and signal sensors.");
/* TODO future ambitions 
  p("    read [mode] [arg1] [arg2] [...]");
  p("      Read the current light output.");
  p("      If [mode] is not specified, read once.");
  p("      If [mode] is `loop`, read once ever [arg1] ms [arg2] times,");
  p('      where 0 for arg1 means "read as fast as possible"');
  p('      and 0 for arg2 means "continue reading until told to stop"');
*/
  p('');
  p("    scan <wavelength_begin> <wavelength_end> [reads per nm]");
  p("      Move mirror from <wavelength_begin> to <wavelength_end> while");
  p("      while reading [reads per nm] times for every nm moved.");
  p('');
  p('  Flags:');
  p('');
  p("    -d <path>: Specify path to serial device, e.g. /dev/ttyUSB0");
  p("    -p: Plot the results on a graph as they come in");
  p("    -o <filepath>: Output data to csv file");
  p("    -s <path>: Specify path to settings.js file");
  p("    -D: Enable debug output");
  p("    -h: Show this usage info");
  p('');
}

var settings = xtend(require(argv.settings), argv);

if(settings.help) {
  usage(console.log);
  process.exit(0);
}

if(argv._.length < 1) {
  console.error("Error: You must specify a command");
  console.error('');
  usage();
  process.exit(1);
}

var callbacks = []; // callback queue
var cmd = argv._[0]
var args = argv._.slice(1);

var port = new SerialPort({
  path: settings.device,
  baudRate: 115200,
  dataBits: 8,
  parity: 'none',
  stopBits: 1
});
var lineParser = new ReadineParser({ delimiter: '\r\n' })
port.pipe(lineParser);

if(settings.debug) {
  lineParser.on('data', function(data) {
    console.log('[serial rx]', data);
  });
}

function sendLine(data) {
  port.write(data + "\n");
  if(settings.debug) {
    console.log('[serial tx]', data);
  }
}

function goTo(nm, cb) {
  sendLine("goto "+nm);
  callbacks.push(cb);
}

function goToP(nm, cb) {
  sendLine("gotop "+nm);
  callbacks.push(cb);
}

function scan(fd, fromNM, toNM, samplesPerNM, cb) {

  sendLine("scan " + fromNM + ' ' + toNM + ' ' + samplesPerNM);
  
  callbacks.push(function(err, data) {
    if(err) return cb(err);

    if(data) {
      data = data.split(/\s+/);
      data = data.slice(1).join(',');
      if(fd) {
        fs.writeSync(fd, new Buffer.from(data+"\n"));
      } else {
        console.log(data);
      }
    } else {
      if(cb) cb();
    }
  });
}

function read(cb) {
  sendLine("read");
  callbacks.push(function(err, data) {
    if(err) return cb(err);
    var a;
    a = data.split(/\s+/);
    cb(null, a[0], a[1]);
  });
}

lineParser.on('data', function(data) {
  if(data.slice(0, 4) === 'done') {
    var cb = callbacks.splice(0, 1)[0];
    if(cb) cb(null, data.slice(5, data.length));
  } else if(data.slice(0, 4) === 'data') {
    callbacks[0](null, data);
  } else if(data.slice(0, 5) === 'ERROR') {
    var errMsg = data.slice(6, data.length);
    if(cb) {
      cb(new Error(errMsg));
    } else {
      console.error("Error:", errMsg);
    }
  } else {
    console.error("Error: Unexpected response:", data);
  }
});

function wavelengthStr(digits) {
  digits = Math.pow(10, digits || 2);
  if(curWavelength === undefined) {
    return '?';
  }
  
  return (Math.round(curWavelength * digits) / digits).toString();
}

function parseCmd(cmd, args, fd) {

  switch(cmd) {

  case 'scan':
    scan(fd, args[0], args[1], args[2], function(err) {
      if(err) {
        console.error(err);
        process.exit(1);
      }
      if(fd) {
        fs.closeSync(fd);
      }
    });
    break;
    
  case 'goto':
    goTo(args[0]);
    break;
    
  case 'gotop':
    goToP(args[0]);
    break;
    
  case 'read':
    read(function(err, sensor1Val, sensor2Val) {
      if(err) {
        console.error(err);
        process.exit(1);
      }
      if(fd) {
        fs.writeSync(new Buffer.from(wavelengthStr() + ',' + sensor1Val + ',' + sensor2Val + "\n"));
      } else {
        console.log("Sensor 1:", sensor1Val);
        console.log("Sensor 2:", sensor2Val);
      }
    });
    break;
    
  default:
    console.error("Error: Unrecognized command");
    console.error('');
    usage();
    process.exit(1);
  }
}

function postInit(fd) {
  parseCmd(cmd, args, fd);
}

function die(msg) {
  console.error(msg);
  process.exit(1);
}

if(settings.output) {
  fs.open(settings.output, 'w+', 0o644, function(err, fd) {
    if(err) die(msg);

    fs.write(fd, new Buffer.from(csvHeader+"\n"), function(err) {
      if(err) die(msg);

      postInit(fd);
    });
    
  });
} else {
  postInit();
}
