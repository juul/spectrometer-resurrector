
This is the bill of materials for the Spectrometer Ressurector.

# Spectrometer

Model: Shimadzu SPD-6AV
Quantity: 1
Price: ~$200

You can find these on US ebay for under $200 shipped. Try to make sure you get one with a working deuterium bulb. The deuterium light should light up about 20 seconds after power on if the bulb is working.

# Computer and/or 5V USB power supply

You will of course need a computer to control the spectrometer and display the readings. If you are using the command line utility and communicating with the spectrometer over USB then you won't need anything else. Your computer will provide power for the electronics (other than the stepper motor). If you plan to use this over wifi and don't want your computer to be tethered to the spectrometer with a USB cable then you can use any 1A or greater USB power supply instead of powering from your computer.

# Electronic modules

## 24 VDC 1A power supply

Quantity: 1
Price: < $10 

Anything off ebay or aliexpress or repurposed from other equipment is fine here. The 24 V is required for the stepper motor.

Here's an example: https://www.aliexpress.us/item/2255801087698437.html

## ESP32 dev board

Model: ESP-WROOM-32 development board
Quantity: 1
Price: $5-$10

These can be found on many sites including ebay, amazon and aliexpress.

## A4988 stepper motor driver module

Quantity: 1
Cost: A few $

Can be found on ebay, amazon or aliexpress.

## 5 VDC input to split +/- 12 VDC output module

Quantity: 1
Price: ~$3

This is a small module that takes 5 VDC input and outputs both pluse and minus 12 VDC. This is used to power the operational amplifiers for the photodiode output.

You can buy them on aliexpress or ebay. Here is one example:

https://www.aliexpress.us/item/3256801393688976.html

but if that item no longer exists then here are some of the keywords to find the module:

```
Power Supply Module 2.8V to 5.5V Input Plus or Minus 12V Output 5V
```

# Electronic components

These can be bought from many places but I get stuff like protoboard and pin headers from aliexpress and actual components from digikey or mouser.

## LM358P dual op amp

Cost: < $1
Quantity: 1

Buy from digikey, mouser or ebay.

## 80 x 120 mm double-sided proto board

The pre-tinned green stuff with 2.54 mm hole spacing.
It should have minimum 42 x 30 holes.

## pin headers female - 2.54 mm 

Cost: A few $ at most
Quantity: 64 pins

## pin headers male - 2.54 mm pin

Cost: A few $ at most
Quantity: 40 pins

## Electrolytic capacitor - 100 uF 25 V

Cost: < $1
Quantity: 1

Exact capacitor value is not super important here, just ensure it's at least 25 V. Higher would be better.

Buy from e.g. digikey or mouser.

## Resistors

Normal 5% through-hole resistors. You will need:

* 3x 10k ohm
* 2x 4.7k ohm
* 2x 33k ohm
* 1x 47k ohm

Buy from e.g. sparkfun.com as part of a kit or digikey/mouser individually.

# DC female barrel jack connector

* Quantity: 1
* Price: < $3

Get whichever one matches the male barrel jack on your power supply.

You can find these on ebay, aliexpress and digikey/mouser.

# Wire

You'll need some insulated and uninsulated unstranded wire. If you don't have uninsulated you can simply strip some insulated wire. The uninsulated wire is used to lay down the traces on the protoboard.



