/*
 * Licensed under GPLv3
 * Copyright 2019 Marc Juul Christoffersen
 */

#include <math.h>
#include <stdlib.h>

#define CMD_LEN_MAX 256
#define DEBUG_MSG_LEN_MAX 256

#define uvSensor1Pin 14
#define uvSensor2Pin 26
#define rotateOpticalPin 33
#define linearOpticalPin 12
#define stepPin 33
#define dirPin 32
#define enablePin 25

#define STEPS_PER_ROTATION (200) // This is a 1.8 degree per step motor
#define MICROSTEPPING (16) // Using 1/16th microstepping
#define MICROSTEPS_PER_ROTATION (STEPS_PER_ROTATION * MICROSTEPPING)
#define SLACK (50) // Amount of mechanical backlash/play in the system, in steps
#define SLACK_MICROSTEPS (SLACK * MICROSTEPPING)

#define LIGHT (1)
#define DARK (0)
#define INDETERMINATE (2)

#define LEFT (LOW)
#define RIGHT (HIGH)

#define SAFE_SPEED (230) // highest speed that gives good precision
#define HOME_NM (90.4749) // What nm measurement corresponds to home position
#define MICROSTEPS_PER_NM (31.890194) // How many microsteps to move one nm
#define HOME_MICROSTEPS (
#define SLACK_NM (SLACK_MICROSTEPS / MICROSTEPS_PER_NM)

int debugEnabled = 0;
int isHomed = 0; // has the system been homed after boot?
int pos; // current position in microsteps

int checkIfRotating(int direction);
int home(int speed);
int move_and_call(int microsteps, int speed, int(*callback)(int position), int microstepsPerCallback);
int gotoWavelengthPrecise(double wavelength, int speed);
int microstepDistance(double wavelength);

void debug(const char* format, ...)
{
  if(!debugEnabled) return;
  
  char fmt[DEBUG_MSG_LEN_MAX];
  char str[DEBUG_MSG_LEN_MAX];
  
  snprintf(fmt, DEBUG_MSG_LEN_MAX, "[debug] %s\r\n", format);
  
  va_list args;
  va_start(args, format);
  vsnprintf(str, DEBUG_MSG_LEN_MAX, fmt, args);
  va_end(args);

  Serial.printf(str);
}

void setup() {
  
  Serial.begin(115200);

  // Declare pins as output:
  pinMode(enablePin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(linearOpticalPin, INPUT);
  pinMode(uvSensor1Pin, INPUT);
  pinMode(uvSensor2Pin, INPUT);
  
  // enable the stepper controller
  digitalWrite(enablePin, LOW);

  Serial.printf("\r\nINIT\r\n");
}

void reply_error(int number, char* msg) {
  Serial.printf("ERROR %d %s\r\n", number, msg);
}

// speed: lower number is higher. from 100 to 1000 are good values
// assuming stepper motor controller configured for 1/16 microsteps and motor is 200 steps per revolution
int moveOneMicroStep(int direction, int speed) {
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(speed);
  digitalWrite(stepPin, LOW);
  delayMicroseconds(speed);
  
  if(checkIfRotating(direction) < 0) {
    return -1;
  }
  return 0;
}

int move(int microsteps, int speed) {
  return move_and_call(microsteps, speed, NULL, 0);
}

// move the specified number of microsteps while calling the callback every microstepsPerCallback
int move_and_call(int microsteps, int speed, int(*callback)(int position), int microstepsPerCallback) {

  int ret;
  int direction = RIGHT;
  if(microsteps < 0) {
    direction = LEFT;
    microsteps = -microsteps;
  }
  digitalWrite(dirPin, direction);
  
  int i;
  for(i = 0; i < microsteps; i++) {
    moveOneMicroStep(direction, speed);
    if(callback && microstepsPerCallback) {
      if(i % microstepsPerCallback == 0) {
        ret = callback(pos + i);
        if(ret < 0) return ret;
      }
    }
  }

  if(direction == LEFT) {
    pos -= microsteps;
  } else {
    pos += microsteps;
  }
  return 0;
}


// This function should be called after every (micro)step
// If it returns < 0 then that means the stepper motor moved more than one rotation
// without the rotation sensor detecting the rotation.
// That means something is seriously wrong (e.g. hit a wall, stepper motor broken or rotation sensor broken)
// TODO this function is not yet working
int checkIfRotating(int direction) {
  /*
  static int travel = 0; // how far have we traveled since last seeing a gap
  int sensorState;
  
  if(direction == LEFT) {
    travel--;
  } else {
    travel++;
  }

  sensorState = getSensorState(rotateOpticalPin, 1);
  if(sensorState == LIGHT) {
    travel = 0;
  }
  
  // Stepper motor traveled too far without seeing the gap
  if(abs(travel) > MICROSTEPS_PER_ROTATION) {
    Serial.println("Error: Stepper motor should have completed an entire rotation but the rotation sensor doesn't agree.");
 Serial.println("Check if stepper motor hit a wall or stopped moving, otherwise rotation sensor may be broken.");
    return -1;
  }
  return 0;
  */
}

int getSensorState(int sensorPin, int definite) {
  int senseVal;
  
  senseVal = analogRead(sensorPin);

  if(senseVal > 3000) {
    return LIGHT;
  } else if(senseVal < 500 || definite) {
    return DARK;
  } else {
    return INDETERMINATE;
  }
}

// get LIGHT if DARK and DARK if LIGHT and INDETERMINATE if INDETERMINATE
int oppositeState(int state) {
  return state ^ 1;
}

// like move() but takes the pin number of a sensor and stops when sensor detects a move from light to dark or vice-versa
// desiredState: LIGHT or DARK
// forceMove: if non-zero then begin moving even if state is already what's desired
int moveUntilTransition(int microsteps, int direction, int speed, int sensorPin, int desiredState, int forceMove) {
  int sensorState;
  int i;
  int traveled = 0;
  
  digitalWrite(dirPin, direction);

  if(microsteps == 0) {
    microsteps = -1;
  }

  while(microsteps) {
    
    sensorState = getSensorState(sensorPin, 0);

    if(sensorState == desiredState && !forceMove) {
      return traveled;
    } else if(sensorState == oppositeState(desiredState) && forceMove) {
      forceMove = 0;
    }
    
    if(moveOneMicroStep(direction, speed) < 0) {
      return -1;
    }
    traveled++;

    if(microsteps > 0) {
      microsteps--;
    }
  }
  return -1;
}

void printState(int state) {
  if(state == DARK) {
    Serial.printf("Dark\r\n");
  } else if(state == LIGHT) {
    Serial.printf("Light\r\n");
  } else {
    Serial.printf("Indeterminate\r\n");
  }
}

// TODO we can remove the position tracking stuff now
//      since that's handled by the global var `pos`

// ensure stepper motor assembly is in a known location (home)
int home(int speed) {
  /*
    This is what the sensor sees as the linear assembly moved from left to right:
    
    |very small gap of light|long section of black|very small gap of light|small section of black|small gap of light|
  */
  
  int ret;
  int sensorState;
  int travel = 0;

  sensorState = getSensorState(linearOpticalPin, 0);

  debug("Begin homing");
  //  printState(sensorState);

  // If we see light or are on a boundary, go right for a bit.
  // If we don't very quickly transition to dark
  // then we are close to the right end and need to stop and reverse.
  if(sensorState != DARK) { 
    debug("Starting in light area");
    ret = moveUntilTransition(4000, RIGHT, speed, linearOpticalPin, DARK, 0);
    if(ret < 0) { // we are close to the right side
      travel += 4000;
      debug("We are close to the right side");
      ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, DARK, 0);
      travel -= ret;
      ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
      travel -= ret;
      // now at left side of slit
      goto success;
    } else {
      travel += ret;
      debug("We were either all the way to the left or on the slit");
      //      ret = moveUntilTransition(0, RIGHT, 100, linearOpticalPin, DARK, 0);
      ret = moveUntilTransition(10000, RIGHT, speed, linearOpticalPin, LIGHT, 0);
      if(ret < 0) {
        travel += 10000;
        debug("We were on the slit");
        ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
        travel -= ret;
        // we are now on the left side of the slit
        goto success;
      } else {
        travel += ret;
        debug("We were all the way to the left");
        // we are now on the right side of the slit
        ret = moveUntilTransition(0, RIGHT, speed, linearOpticalPin, DARK, 0);
        travel += ret;
        ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
        travel -= ret;
        // we are now on the left side of the slit
        goto success;
      }
    }
  } else { // starting in a dark area
    // go right for a bit and see if we hit light
    ret = moveUntilTransition(6000, RIGHT, speed, linearOpticalPin, LIGHT, 0);
    if(ret < 0) {
      travel += 6000;
      debug("We are in the large dark area 1");
      // we are in the large dark area
      ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
      travel -= ret;
      // we are now on the left side of the slit
        goto success;
    } else {
      travel += ret;
      debug("either we were at the left edge of the large dark area or in the small dark area");
      // either we were at the left edge of the large dark area
      // or somewhere in the small dark area.
      // Go back a bit past where we were and see if we hit light
      ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, DARK, 0);
      travel -= ret;
      ret = moveUntilTransition(9000, LEFT, speed, linearOpticalPin, LIGHT, 0);
      if(ret < 0) {
        travel -= 9000;
        debug("We are in the large dark area 2");
        // we are in the large dark area
        ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
        travel -= ret;
        // we are now on the left side of the slit
        goto success;
      } else {
        travel -= ret;
        debug("We were in the small dark area 3");
        // we were in the small dark area
        ret = moveUntilTransition(0, RIGHT, speed, linearOpticalPin, DARK, 0);
        travel += ret;
        ret = moveUntilTransition(0, RIGHT, speed, linearOpticalPin, LIGHT, 0);
        travel += ret;
        // we are now on the right side of the slit
        ret = moveUntilTransition(0, RIGHT, speed, linearOpticalPin, DARK, 0);
        travel += ret;
        ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
        travel -= ret;
        // we are now on the left side of the slit
        goto success;
      }
    }
  }
  
 success:
  debug("Home!");
  isHomed = 1;
  pos = 0;
  return travel;  
}

// convert microsteps to wavelength
double microstepsToWavelength(int microsteps) {
  return (microsteps / MICROSTEPS_PER_NM) + HOME_NM;
}

// convert microsteps to wavelength
int wavelengthToMicrosteps(double wavelength) {
  return round(MICROSTEPS_PER_NM * (wavelength - HOME_NM));
}

// report the current sensor data over serial
int scanCallback(int position) {
  int val1;
  int val2;
  double curWavelength = round(microstepsToWavelength(position) * 100) / 100;

  val1 = analogRead(uvSensor1Pin);
  val2 = analogRead(uvSensor2Pin);
  
  Serial.printf("data %f %d %d\r\n", curWavelength, val1, val2);
  
  return 0;
}

int scan(double fromWavelength, double toWavelength, int speed, int samplesPerNanometer) {
  int ret;
  int microsteps;
  int direction;
  int microstepsPerSample;

  
  if(samplesPerNanometer > MICROSTEPS_PER_NM) {
    Serial.printf("ERROR 9001 Too many samples per nanometer. Maximum is %f\r\n", MICROSTEPS_PER_NM);
    return -1;
  }

  microstepsPerSample = ceil(MICROSTEPS_PER_NM / samplesPerNanometer);
  
  ret = gotoWavelengthPrecise(fromWavelength, speed);
  if(ret < 0) return ret;

  microsteps = microstepDistance(toWavelength);

  // begin moving the mirror, calling scanCallback every microstepsPerSample
  ret = move_and_call(microsteps, speed, *scanCallback, microstepsPerSample);
  if(ret < 0) return ret;
  
  Serial.printf("done\r\n");
}

// calculate number of microsteps between current position and specified wavelength
int microstepDistance(double wavelength) {
  int targetPos = wavelengthToMicrosteps(wavelength);
  return targetPos - pos;  
}

int gotoWavelength(double wavelength, int speed) {
  int toMove = microstepDistance(wavelength);
  return move(toMove, speed);
}

// Use information about where we are to home quicker
int quickHome(int speed) {
  //  int toMove;
  int ret;
  
  if(!isHomed) {
    return home(speed);
  }

  // go just a bit to the right of home
  ret = gotoWavelength(ceil(HOME_NM + SLACK_NM), speed);
  if(ret < 0) {
    return ret;
  }

  // move left until we hit home
  ret = moveUntilTransition(0, LEFT, speed, linearOpticalPin, LIGHT, 0);
  if(ret < 0) {
    return ret;
  }

  pos = 0;
  
  return 0;
}

// First move to home,
// then move left to (0 nm - slack),
// then move right until the target wavelength.
// This should give repeatable results and thus more precision
int gotoWavelengthPrecise(double wavelength, int speed) {
  int ret;
  int toMove;

  // Go to home
  ret = quickHome(speed);
  if(ret < 0) {
    return ret;
  }

  // Go just a bit left of 150 nm
  toMove = microstepDistance(150);
  ret = move(toMove - SLACK_MICROSTEPS, speed);
  if(ret < 0) {
    return ret;
  }

  // Now go to the actual wavelength
  toMove = microstepDistance(wavelength);

  return move(toMove, speed);
}

int parseCommand(char* cmd) {
  static int speed = SAFE_SPEED;
  int prevPos;
  int travel_to_home;
  int val;
  int val2;
  int ret;
  long int fromWavelength;
  long int toWavelength;
  long int samplesPerNM;
  char* endptr;
  
  if(strncmp(cmd, "debug", 5) == 0) {
    val = atoi(cmd+6);
    if(val) {
      debugEnabled = 0;
    } else {
      debugEnabled = 1;
    }
    Serial.printf("done %d\r\n", debugEnabled);
  } else if(strcmp(cmd, "l") == 0) {
    move(-1000, speed);
    Serial.printf("done %d\r\n", pos);
  } else if(strcmp(cmd, "r") == 0) {
    move(1000, speed);
    Serial.printf("done %d\r\n", pos);
  } else if(strncmp(cmd, "home", 4) == 0) {
    Serial.printf("quick home\r\n");
    quickHome(speed);
  } else if(strcmp(cmd, "h") == 0) {
    Serial.printf("home\r\n");
    prevPos = pos;
    travel_to_home = home(speed);
    //    Serial.printf("Pos %d, Travel %d, Offset %d\r\n", prevPos, travel_to_home, travel_to_home + pos);
  } else if(strcmp(cmd, "d") == 0) {
    speed = speed + 10;
    if(speed > 1000) {
      speed = 1000;
    }
    Serial.printf("done %d\r\n", speed);
  } else if(strcmp(cmd, "u") == 0) {
    speed = speed - 10;
    if(speed < 20) {
      speed = 20;
    }
    Serial.printf("done %d\r\n", speed);
  } else if(strcmp(cmd, "read1") == 0) {
    val = analogRead(uvSensor1Pin);
    Serial.printf("done %d\r\n", val);
  } else if(strcmp(cmd, "read2") == 0) {
    val = analogRead(uvSensor1Pin);
    Serial.printf("done %d\r\n", val);

  } else if(strcmp(cmd, "read") == 0) {
    val = analogRead(uvSensor1Pin);
    val2 = analogRead(uvSensor2Pin);
    Serial.printf("done %d %d\r\n", val, val2);

  } else if(strncmp(cmd, "gotop", 5) == 0) { // precise goto

    val = atoi(cmd+6);
    Serial.printf("gotop %d\r\n", val); 
    if(val < 180 || val > 800) {
      reply_error(7, "Specified wavelength is out of bounds. Range is 180 to 800.");
      return -1;
    }

    // TODO add ability to accept floating point values
    ret = gotoWavelengthPrecise((double) val, speed);
    Serial.printf("done\r\n");
  
  } else if(strncmp(cmd, "goto", 4) == 0) {
    val = atoi(cmd+5);

    if(val < 180 || val > 800) {
      reply_error(7, "Specified wavelength is out of bounds. Range is 180 to 800.");
      return -1;
    }

    // TODO add ability to accept floating point values
    ret = gotoWavelength((double) val, speed);
    Serial.printf("done\r\n");
    
  } else if(strncmp(cmd, "scan", 4) == 0) { // precise goto
    // incoming command is expected to be in the form:
    //   "scan <fromWaveLength> <toWavelength> <samplesPerNanometer>"
    
    fromWavelength = strtol(cmd+5, &endptr, 10);
    toWavelength = strtol(endptr+1, &endptr, 10);
    samplesPerNM = strtol(endptr+1, &endptr, 10);
                            
    ret = scan(fromWavelength, toWavelength, speed, samplesPerNM);
    if(ret < 0) {
      // TODO remove this
      // error messages are expected to be reported where they happen
      // and we only want one error message per error
      reply_error(0, "Unknown error during scan");
    }
    
  } else {
    reply_error(2, "Unrecognized command");
  }    
  return 0;
}

int readLine() {

  int c;
  static char buf[CMD_LEN_MAX];
  static int ignore_line = 0;
  static int i = 0;

  while(Serial.available()) {
    c = Serial.read();
    if(c < 0) {
      reply_error(66, "Serial data available but could not read it");
      return -1;
    }
    
    if(c == '\r' || c == '\n') {
      buf[i] = '\0';
      i = 0;
      if(ignore_line) {
        ignore_line = 0;
        continue;
      }
      parseCommand(buf);
      continue;
    }
    buf[i] = (char) c;

    i++;
    if(i >= CMD_LEN_MAX) {
      reply_error(1, "Command too long");
      ignore_line = 1;
      i = 0;
      return -1;
    }
  }
}


void loop() {

  readLine();

  delay(50);
  
  /*
  int senseVal = analogRead(14);
  Serial.println(senseVal);
  senseVal = analogRead(26);
  Serial.println(senseVal);
  senseVal = analogRead(linearOpticalPin);
  Serial.printf("motor optical: %d\n", senseVal);
  Serial.printf("---"); 
  delay(1000);
*/  
    /*
  static int direction = RIGHT;
  static int keep_running = 1;

  if(!keep_running) {
    Serial.println("Error detected so stopped everything");
    delay(1000);
    return;
  }
  
  //  if(moveUntilSense(0, direction, 100, linearOpticalPin, 0) < 0) {
  //    keep_running = 0;
  //    return;
  //  }
  
  int senseVal = analogRead(linearOpticalPin);
  Serial.println(senseVal);
  
  if(direction == LEFT) {
    direction = RIGHT;
  } else {
    direction = LEFT;
  }

  move(100, LEFT, 800);
  //  moveOneMicroStep(direction, 500);
  
  //  delay(1000);
  */
}
