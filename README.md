
Spectrometer-resurrector aims to be an open replacement for the control circuitry and software for off-the-shelf older generation spectrophotometers that are cheaply available second hand. Many of these units have excellent optics and electro-mechanics but contain outdated control/interface electronics and software. 

Think of this as a brain transplant that turns an old proprietary spectrometer into a modern, wifi-enabled open spectrometer.

This project is a work in progress! If you are considering using data obtained using this project for actual research please check in with the developer first in order to understand possible pitfalls and limitations.

The hardware uses an ESP32 with a control and logging GUI served up as a web interface by the ESP32. The main circuitboard currently includes a stepper motor controller for adjusting the wavelength and a split +/- 12 V supply for powering analog photodetector circuitry.

The goal is to support a variety of spectrophotometers. Currently only a single Shimadzu HPLC spectrometer is supported.

A longer term dream is to also control other devices such as liquid chromatography pumps, mixers, valves, etc. and make it easy to build full LC or HPLC systems out of mismatched second-hand modules. It would also be interesting to complement this project with an open opto-mechanical design such that the off-the-shelf hardware is fully eliminated.

The shimadzu spectrometer was chosen because its construction is representative of a class of affordable pre-DADS spectrometers, and because it frequently pops up on ebay for less than $200.

# Building the hardware

HIGH VOLTAGE WARNING: This hardware includes high voltage circuitry. Though we only measured 190 VDC, it is very possible that voltages above 400 are present in some/all of these devices. Don't even take the case off these devices if you aren't experienced in the safe handling of high voltage devices.

DANGEROUS RADIATION WARNING: Deuterium arc lamps generate UVC radiation. This is _very_ dangerous to your eyes and skin. It can destroy your eyes and give you cancer. Getting UVC on your skin is not like getting a sunburn. You generally only encounter this type of light when you stand under a hole in the ozone layer or when arc-welding, which is why you need to wear protective clothing and masks when you weld. The good news is that polycarbonate is great at blocking UVC so UVC face shields are not expensive. Make sure you cover your skin and not just your face/eyes.

The Bill of Materials is available in `BOM.md`.

The current iteration of the design is built on protoboard. Once it has been finalized we plan to publish a KiCad PCB layout and gerber files but for now assembly involved manually running wires on protoboard. Luckily it is not a very complicated circuit.

The schematic is available in KiCad format in the `kicad/` directory. 

When opening up the case of the Shimadzu spectrometer you will see that the inside is split into two halves. One is full of circuit boards and the other is a black box-like thing. The black box is the optical assembly that contains the light sources, optics, photodetectors, stepper motor to select wavelength and a few photodetectors that give feedback on the stepper motor position.

The next step is to construct the circuit as described in the KiCad schematic on a piece of protoboard. This circuit has four external connectors. One for 24 V input where a barrel jack should be soldered. One 10-pin connector where the 10-pin cable from the black box should be attached (this is the signal output from the absorbance light sensors). One 6-pin connector where the stepper motor cable should be attached. And finally one four-pin connector where one of the photodiode position sensor should be attached. The sensor to attach is the one located toward the middle of the black box. When hooking up the sensor cable be careful not to reverse the polarity as it will permanently destroy the sensor. The colors of the individual wires are noted in the schematic. If you still manage to destroy one replacements are fortunately still being produced and they are not expensive.

# Software setup

There are three parts to the software. The firmware, the command-line client and the web app. The firmware is compiled and flashed onto the ESP32, the command-line client runs on a *nix machine via node.js and the web app is an optional component which is built using node.js and then flashed onto the ESP32.

This guide assumes that you're running a Debian-based operating system.

## Flashing the firmware

You will need to [install the Arduino IDE and ESP32 board support](https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html).

Then a copy of `makeEspArduino` needs to be downloaded:

```
cd esp32/
./fetch.sh
```

You will need to change the `ESP_ROOT=` argument in `build.sh` and `flash.sh`. It's a good idea to make your own copies of these scripts before modifying.

Now running `./build.sh` should compile the firmware and running `./flash.sh` should flash the firmware onto the ESP32 assuming you the USB cable plugged in.

## Command-line client setup

You will need to have node.js installed. You can install the latest long-term support version by first [installing nvm](https://github.com/nvm-sh/nvm#installing-and-updating), then opening up a new terminal and running:

```
nvm install --lts
```

For reference, node.js version v16.16.0 is the latest version that has been tested as of this writing but newer versions are likely to work unless you are reading this in the distant future.

Now `cd` into the `client/` directory and install the required node packages using:

```
npm install
```

Now you need to copy `settings.js.example`:

```
cp setings.js.example settings.js
```

Make sure to edit your `settings.js` changing the device path for the ESP32 USB serial device if it isn't `/dev/ttyUSB0`.

# Usage

Currently scanning is accomplished using a USB connection to the ESP32 board and a command-line tool.

Calibration and plotting is handled externally and the procedure is described below.

A web app is being developed that will automate calibration and plotting, simplifying common operations.

## Running the command-line client

Make sure your computer is connected to the ESP32 development module over USB, that the light source is powered on and that the 24 V power supply is connected to your resurrector board.

Now cd into `client/` and run:

```
./bin/cmd.js
```

You will see the following output:

```
Error: You must specify a command

Usage: cmd.js <command> [arg1] [arg2] [...]

  Commands:

    goto <wavelength>
      Move mirror to measure at <wavelength> nm

    gotop <wavelength>
      Precise version of `goto` which always re-homes and always
      approaches the location from the same side in an attempt
      to minimize the effects of backlash

    read
      Read the current light output.
      Outputs from both the reference and signal sensors.

    scan <wavelength_begin> <wavelength_end> [reads per nm]
      Move mirror from <wavelength_begin> to <wavelength_end> while
      while reading [reads per nm] times for every nm moved.

  Flags:

    -d <path>: Specify path to serial device, e.g. /dev/ttyUSB0
    -p: Plot the results on a graph as they come in
    -o <filepath>: Output data to csv file
    -s <path>: Specify path to settings.js file
    -D: Enable debug output
    -h: Show this usage info
```

As per the usage explanation, the command lets you move the spectrometer to a specified wavelength (`goto` and `gotop`, read the light output at the current wavelength (`read`) and scan across a range of wavelengths while reading the light output at specified intervals. It should be noted that 

If you are not in a hurry always use `gotop` instead of `goto`.

As an example of how to move to 280 nm and read the light output three times, the commands would be:

```
./bin/cmd.js gotop 280
./bin/cmd.js read 
```

To scan across a range of wavelengths and read once for each nm, outputting the result to a CSV file, do:

```
./bin/cmd.js -o output.csv scan 200 300 1
```

## Output format

The output format from the `scan` command is a CSV formatted with a single row for the column names and each row containing a reading at a specific wavelength. Here is an example:

```
Wavelength,Sample,Reference
200,1500,1507
205,1400,1506
210,1300,1502
215,1200,1499
220,1250,1509
```

The reference column is the output from the photodiode sensor that isn't obscured by the sample and as such the Sample values should be subtracted form their corresponding Reference values to give usable data. This assumes you are accounting for calibration (see next section).

## Calibration

The Reference column in the output data is the signal from the photodiode that isn't obscured by the current sample, while the Sample column is the signal from the photodiode that _is_ obscured by the current sample.

Unfortunately the light output from the two light sources isn't constant across all wavelengths, so calibration data must be created and used to offset the actual output.

First load pure water into the sample chamber (flow cell).

Now use the command-line tool to run a scan across all wavelengths of interest, saving the output to a CSV file. You might want to repeat this multiple times and take the average of each value.

Save these files as your calibration data. When making a reading at a specific wavelength with a sample of interest, subtract the reported Sample and Reference values from the corresponding values in your calibration CSV file.

This assumes the photodiode response is relatively linear and that its linearity is similar across all wavelengths. This may not be the case and we have not yet characterized the linearity of the photodiodes. Unfortunately the photodiodes do not have a model number so we have not been able to obtain a datasheet. This is an important avenue for future improvement and replacing the photodiodes might be the best way forward.

# Resurrector electronics

## Stepper motor controller

Model: Polulu A4988 black edition

* Using 24 V 2 A power supply from external supply
* Set current limit to 1 A
* Set micro-stepping to 1/16th
* Using 100 uF capacitor on power supply input.

## Microcontroller

* Model: ESP32 (ESP-WROOM)

# Shimadzu SPD-6AV UV/Vis HPLC

This type of HPLC spectrometer can only monitor a single wavelength at any given time. It uses deuterium and tungsten lamps and is capable of measuring down to at least 200 nm, possibly lower. 

It works by physically moving a refraction grating using a stepper motor to shine the selected wavelength through an HPLC flow cell and onto a detector. Two detectors are used such that one sees the light through the flow cell and one bypasses the flow cell and is used as a reference. A mirror is used to switch between UV and visible. This mirror was originally manually controlled so another motor would need to be added if automatic control is needed.

The SPD-6AV dates from around 1985 but was produced until at least 1992.

## Detector module

Uses two detectors (model unknown) and two AD549KH op-amps.

## Stepper motor

Model: Japan Servo Co. KP56HM2-013

Specs:

* 24 V
* 1.8 deg/step
* Hybrid stepper motor

## Wiring

* red | red-stripe: coil 1
   * black: center tap for coil 1

* green-stripe | green: coil 2
   * white: center tap for coil 2

My wiring:

* brown | white: coil 1
   * red: center tap for coil 1

* orange | yellow: coil 2
   * blue: center tap for coil 2

# Photo interruption sensors

Model: Omron EE-SV3 Photomicrosensor

__BEWARE:__ The plugs for the sensors are not all the same. The one for the mirror has two wires switched.

* Yellow: LED+ | To 5 V through a 470 Ohm resistor
* Blue: LED- | To ground
* Red: Photo-transistor collector | To 3.3 V 
* White: Photo-transistor emitter | To analog in and to ground via 10 kOhm resistor

## Precision

To move the mirror 1 nm the motor moves the horizontal slider about 0.019875 mm per nm.

1000 microsteps moves the slider about 1.19 mm.

Precision was measured by homing, then moving a random number of microsteps left or right, then re-homing and checking how many microsteps the system thought it moved to get back home.

Largest error observed in repeated homing with speed 80 is 75 microsteps. That's about 4.5 nm of error. Error was very sporadic so it may indicate the motor is missing microsteps here and there.

Largest error with speed 230: 14. We got exactly 14 many times, so that's probably just some mechanical backlash. That's and error of about 0.84 nm. This seems acceptable.

# ToDo

State of this project is that many of the pieces are almost there but a few important bits are still missing (e.g. calibration and logging) and then comes the work of integration, testing and improving UI/UX.

Important:

* Finish command-line tool to start spectrophotometer, collect data and plot it
* Document how to accomplish one-time calibration
* Document how to fool original control electronics to power on the light source
* Complete electronics schematic
* Make a PCB layout

Non-critical / future:

* Stepper control code needs to figure out where it is on bootup (if moved)
* Implemented easy guided one-time calibration procedure
* Hook up remaining photo interruption sensors
* Add second stepper motor for automatic mirror control?
* Replace burnt-out photo interruption sensor on juul's spec
* Design light source power supply 

## WebServer and WebSocket server on ESP32

ESPAsyncWebServer does everything we need, including WebSockets

https://github.com/me-no-dev/ESPAsyncWebServer


# Light source

The original Shimadzu spectrometer's light source is a combination of a normal tungsten filament bulb and a deuterium arc lamp.


## UV light source

The deuterium arc bulb is a `Hamamatsu model L2D2 L6381`. The `L` indicates that it is a long lifetime bulb though Shimadzu only guarantees the bulb for 500 hours.

I haven't been able to find the official specification for the L6381 (though it appears that Shimadzu may still sell them), there are [specs available for the L6301](https://web.archive.org/web/20220916085506/https://www.hamamatsu.com/jp/ja/product/light-and-radiation-sources/lamp/deuterium-lamp/L6301.html) and there is a nice [pdf spec sheet](https://web.archive.org/web/20220315192333/https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/etd/D2lamps_TLS1017E.pdf) for the L6xxx series bulbs which all seem fairly similar. Here is a best guess from that:

* Wavelength: 185 nm to 400 nm
* Filament warm-up: 2.5 V +- 0.25 at 4 A for 20 seconds
* Filament operating: 1.0 V at 1.8 A or 1.7 at 3.3 A (bulb dependent)
* Tube voltage (starting): 400 V dc
* Tube voltage (running): 80 V dc
* Anode current: 300 mA +- 30

Measuring what the actual spectrometer does we instead get:

* Filament warm-up: 2.5 V ac
* Filament operating: 0.7 V dc (but unsure if this is real. need to measure current as well)
* Tube voltage (starting): ~190 V dc
* Tube voltage (running): 72 V dc

The 190 V to start seems surprisingly low, yet the deuterium bulb lights up!. It's also odd that the filament supply would switch from AC to DC so it seems possible that no power is actually flowing through the filament during operation.

Interestingly [the wikipedia page on deuterium arc lamps](https://en.wikipedia.org/w/index.php?title=Deuterium_arc_lamp&oldid=1057664433#Principle_of_operation) agrees with our hypothesis that the 0.7 filament voltage could be a measurement artifact, stating that: "Because the discharge process produces its own heat, the heater is turned down after discharge begins." 

It could be that Shimadzu engineers decided to run the lamp well below spec for whatever reason, either to prolong bulb lifetime or perhaps to do the opposite and ensure that a dying bulb won't light up at all.

## Tungsten light source

ToDo investigate voltage and type

## Light source power supply

The power supply and logic seems annoyingly deeply integrated into the electronics that we're replacing. We need to document how to fool it into powering on the two bulbs even when the optical assembly is not attached.

Longer term we should design a replacement open hardware module that can power on the two bulbs directly. The design for the deuterium arc lamp supply is the most complicated. A design with a single buck converter and a single boost converter should be able to take 24 V input (needed for stepper motor) and buck it down to 2.5 V or lower for the filament heat-up and boost it to the 70-80 V and 190-400 V needed for running and start-up. These can be controlled by two PWM pins from the ESP32.

# Future improvements

Right now the built-in ADC of the ESP32 is being used to read the photodiode output with a simple resistor divider to get the voltage into the ADC range. This is not very precise. Switching to an external ADC component is a good idea.

Characterize linearity of the photodiodes per wavelength and if it's horrible consider replacing with different photodiodes.

Understand if the op amp used is linear and low noise enough.

# License and copyright

* Microcontroller firmware source code licensed under GPLv3
* Web app source code licensed under AGPLv3
* Hardware licensed under TAPR v1.0

See LICENSE files in `esp32/`, `web/` and `kicad/` directories for full license text.

* Copyright 2019 and 2022 Marc Juul Christoffersen